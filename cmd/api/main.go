package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"reflect"
	"time"

	"albion-kills-api/internal/endpoints"
	"albion-kills-api/internal/models"

	"github.com/Depado/ginprom"
	"github.com/JGLTechnologies/gin-rate-limit"
	"github.com/getsentry/sentry-go"
	sentrygin "github.com/getsentry/sentry-go/gin"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	"github.com/loopfz/gadgeto/tonic"
	cors "github.com/rs/cors/wrapper/gin"
	"github.com/wI2L/fizz"
	"github.com/wI2L/fizz/openapi"
)

var db *sqlx.DB
var itemNameMap map[string]string

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Warning: Failed to load .env file")
	}

	dbConnect()
	loadItemMetadata()
	initSentry()
	defer sentry.Flush(time.Second * 2)

	r := setupRouter()
	r.Run(":8080")
}

func loadItemMetadata() {
	itemsFile, err := os.Open("items.json")
	if err != nil {
		panic(err)
	}

	defer itemsFile.Close()

	itemsBytes, err := ioutil.ReadAll(itemsFile)

	if err != nil {
		panic(err)
	}

	var items []models.ItemMetadata
	json.Unmarshal(itemsBytes, &items)

	itemNameMap = make(map[string]string)
	for _, item := range items {
		itemNameMap[item.UniqueName] = item.LocalizedNames.ENUS
	}

	log.Printf("Loaded %d items into name lookup map\n", len(itemNameMap))
}

func dbConnect() {
	var err error
	db, err = sqlx.Open("mysql", os.Getenv("ALBION_KILLS_API_DB"))
	if err != nil {
		panic(err)
	}

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)
}

func initSentry() {
	// SENTRY_DSN and SENTRY_ENVIRONMENT must be defined in .env
	if err := sentry.Init(sentry.ClientOptions{}); err != nil {
		fmt.Printf("Sentry initialization failed: %v\n", err)
	}
}

func rateLimiterKeyFunc(c *gin.Context) string {
	return c.ClientIP()
}

func rateLimiterErrorHandler(c *gin.Context, info ratelimit.Info) {
	c.String(429, "Too many requests. Try again in "+time.Until(info.ResetTime).String())
}

func setupRouter() *gin.Engine {
	if os.Getenv("GIN_MODE") == "release" {
		gin.SetMode(gin.ReleaseMode)
	}

	r := gin.Default()

	store := ratelimit.InMemoryStore(&ratelimit.InMemoryOptions{
		Rate:  time.Second,
		Limit: 2,
	})
	rateLimiter := ratelimit.RateLimiter(store, &ratelimit.Options{
		ErrorHandler: rateLimiterErrorHandler,
		KeyFunc:      rateLimiterKeyFunc,
	})

	p := ginprom.New(
		ginprom.Engine(r),
		ginprom.BucketSize([]float64{.005, .01, .025, .05, .1, .25, .5, 1, 2.5, 5, 10}),
	)
	r.Use(p.Instrument())

	if os.Getenv("GIN_MODE") == "release" {
		r.Use(cors.New(cors.Options{
			AllowedOrigins:   []string{"https://editor.swagger.io"},
			AllowedHeaders:   []string{"*"},
			AllowCredentials: true,
		}))
	} else {
		r.Use(cors.Default())
	}

	tonic.SetRenderHook(customTonicRenderHook, "")

	if os.Getenv("SENTRY_ENVIRONMENT") != "" {
		r.Use(sentrygin.New(sentrygin.Options{}))
	}

	r.GET("/status", func(c *gin.Context) {
		c.String(http.StatusOK, "running")
	})

	fizz := fizz.NewFromEngine(r)
	infos := &openapi.Info{
		Title:       "Murder Ledger API",
		Description: "Albion events and statistics for murderledger.com",
		Version:     "1.0.0",
	}
	// Create a new route that serve the OpenAPI spec.
	fizz.GET("/api/openapi.json", nil, fizz.OpenAPI(infos, "json"))

	endpointCtx := &endpoints.EndpointCtx{
		GinEngine:          r,
		Fizz:               fizz,
		DB:                 db,
		ItemNameMap:        itemNameMap,
		TwitchClientId:     os.Getenv("TWITCH_CLIENT_ID"),
		TwitchClientSecret: os.Getenv("TWITCH_CLIENT_SECRET"),
		RateLimiter:        rateLimiter,
	}
	endpoints.Attach(endpointCtx)

	return r
}

func customTonicRenderHook(c *gin.Context, statusCode int, payload interface{}) {
	var status int
	if c.Writer.Written() {
		status = c.Writer.Status()
	} else {
		status = statusCode
	}

	if payload == nil || reflect.ValueOf(payload) == reflect.Zero(reflect.TypeOf(payload)) {
		c.String(status, "")
	} else {
		if gin.IsDebugging() {
			c.IndentedJSON(status, payload)
		} else {
			c.JSON(status, payload)
		}
	}
}
