package syncstat

import (
	"log"
	"sync"
	"time"

	"github.com/jmoiron/sqlx"
)

var Status SyncStatus = SyncStatus{}

const cacheTTL = time.Second * 60

type SyncStatus struct {
	syncDelay  time.Duration
	lastUpdate time.Time
	sync.Mutex
}

func (s *SyncStatus) GetDelay(db *sqlx.DB) time.Duration {
	if s.needsUpdate() {
		s.updateSyncDelay(db)
	}

	return s.syncDelay
}

func (s *SyncStatus) updateSyncDelay(db *sqlx.DB) {
	s.Lock()
	defer s.Unlock()

	if !s.needsUpdate() {
		return
	}

	row := db.QueryRow(`select TIMESTAMPDIFF(SECOND, max(time), NOW()) from events;`)

	var seconds int
	err := row.Scan(&seconds)

	if err != nil {
		log.Println("Failed to lookup sync delay")
		return
	}

	s.syncDelay = time.Second * time.Duration(seconds)
	s.lastUpdate = time.Now()
}

func (s *SyncStatus) needsUpdate() bool {
	return time.Now().Sub(s.lastUpdate) > cacheTTL
}
