package endpoints

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/juju/errors"
	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"
)

type playerSearchResponse struct {
	Results []string `json:"results"`
}

type PlayerSearchParams struct {
	Q string `path:"q" validate:"min=3"`
}

type getPlayerSearch struct {
	db *sqlx.DB
}

func (ep *getPlayerSearch) setup(ctx *EndpointCtx) {
	ep.db = ctx.DB

	ctx.Fizz.GET("/api/player-search/:q", []fizz.OperationOption{
		fizz.Summary("Search for player name"),
		fizz.ID("player_search"),
	}, tonic.Handler(ep.handle, 200))
}

func (ep *getPlayerSearch) handle(c *gin.Context, params *PlayerSearchParams) (*playerSearchResponse, error) {
	results, err := ep.fetchResults(params.Q)

	if err != nil {
		c.Error(fmt.Errorf("Failed searching for player: %v\n", err))
		c.Status(http.StatusInternalServerError)
		return nil, errors.Annotate(err, "Failed search for player")
	}

	return &playerSearchResponse{Results: results}, nil
}

func (ep *getPlayerSearch) fetchResults(q string) ([]string, error) {
	query := `select name from player_fav_weapon where name like concat(?, '%') order by last_update desc limit 8;`
	rows, err := ep.db.Query(query, q)

	if err != nil {
		return nil, err
	}

	results := make([]string, 0)

	for rows.Next() {
		var name string
		err := rows.Scan(&name)
		if err != nil {
			return nil, err
		}

		results = append(results, name)
	}

	return results, nil
}
