package endpoints

import (
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/juju/errors"
	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"
)

type getPlayer2v2Info struct {
	db *sqlx.DB
}

type GetPlayer2v2InfoParams struct {
	Name string `path:"name"`
}

type Player2v2Info struct {
	Partners []BattlePartner `json:"partners"`
	Weapons  []BattleWeapons `json:"weapons"`
}

type BattleWeapons struct {
	Weapon1 string `json:"weapon_1"`
	Weapon2 string `json:"weapon_2"`
	Wins    int    `json:"wins" db:"total"`
	Weapons string `json:"-" db:"weapons"`
}

type BattlePartner struct {
	Name    string  `json:"name" db:"partner"`
	Count   int     `json:"count" db:"total"`
	Wins    int     `json:"wins" db:"wins"`
	WinRate float64 `json:"win_rate" db:"win_rate"`
}

func (ep *getPlayer2v2Info) setup(ctx *EndpointCtx) {
	ep.db = ctx.DB

	ctx.Fizz.GET("/api/players/:name/info/2v2", []fizz.OperationOption{
		fizz.Summary("Get 2v2 info for a player"),
		fizz.Response("404", "Player Not Found", nil, nil, nil),
		fizz.ID("get_player_2v2_info"),
	}, tonic.Handler(ep.handle, 200))
}

func (ep *getPlayer2v2Info) handle(c *gin.Context, params *GetPlayerParams) (*Player2v2Info, error) {
	partners, err := ep.fetchPartners(params.Name)

	if err != nil {
		return nil, errors.Annotate(err, "Failed fetching 2v2 partners")
	}

	weapons, err := ep.fetchWeapons(params.Name)

	if err != nil {
		return nil, errors.Annotate(err, "Failed fetching 2v2 weapons")
	}

	info := &Player2v2Info{
		Partners: partners,
		Weapons:  weapons,
	}

	return info, nil
}

func (ep *getPlayer2v2Info) fetchPartners(name string) ([]BattlePartner, error) {
	query := `
select partner, count(*) as total, sum(win=1) as wins, sum(win=1)/count(*) as win_rate
from (
select
  IF(b.winner_1 = :name, b.winner_2, IF (b.winner_2 = :name, b.winner_1, IF (b.loser_1 = :name, b.loser_2, b.loser_1))) as partner,
  IF(b.winner_1 = :name OR b.winner_2 = :name, 1, 0) as win
from 2v2_hg_battle as b
where (b.winner_1 = :name OR b.winner_2 = :name OR b.loser_1 = :name OR b.loser_2 = :name)
  and time > DATE_SUB(NOW(), INTERVAL 30 DAY)
) as t
group by partner
order by total desc
limit 5;`

	partners := []BattlePartner{}
	rows, err := ep.db.NamedQuery(query, map[string]interface{}{"name": name})

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		partner := BattlePartner{}
		err = rows.StructScan(&partner)
		if err != nil {
			return nil, err
		}
		partners = append(partners, partner)
	}

	return partners, nil
}

func (ep *getPlayer2v2Info) fetchWeapons(name string) ([]BattleWeapons, error) {
	query := `
select weapons, count(*) as total
from (
select
  if(wl1.main_hand_item < wl2.main_hand_item, CONCAT(wl1.main_hand_item, '-', wl2.main_hand_item), CONCAT(wl2.main_hand_item, '-', wl1.main_hand_item)) as weapons
from 2v2_hg_battle as b
join loadouts as wl1 on b.winner_1_loadout = wl1.id
join loadouts as wl2 on b.winner_2_loadout = wl2.id
where (b.winner_1 = :name OR b.winner_2 = :name)
  and time > DATE_SUB(NOW(), INTERVAL 30 DAY)
) as t
group by weapons
order by total desc
limit 5;
    `

	weapons := []BattleWeapons{}
	rows, err := ep.db.NamedQuery(query, map[string]interface{}{"name": name})

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		weapon := BattleWeapons{}
		err = rows.StructScan(&weapon)
		if err != nil {
			return nil, err
		}

		weapons = append(weapons, weapon)
	}

	for i, w := range weapons {
		parts := strings.Split(w.Weapons, "-")
		weapons[i].Weapon1 = parts[0]
		weapons[i].Weapon2 = parts[1] // unsafe
	}

	return weapons, nil
}
