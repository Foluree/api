package endpoints

import (
	"fmt"
	"sync"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/juju/errors"
	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"
)

const cacheTTL = time.Hour * 12

type getWeaponMatrix struct {
	db          *sqlx.DB
	slayerCache *cachedMatrix
	cache       *cachedMatrix
}

type GetWeaponMatrixParams struct {
	Slayer bool `query:"slayer" default:"false" description:"Only include slayer matches"`
}

type WeaponMatrix = map[string]map[string]*matrixDetail

type matrixDetail struct {
	Wins   int `json:"wins"`
	Losses int `json:"losses"`
}

type cachedMatrix struct {
	Matrix     WeaponMatrix `json:"matrix"`
	LastUpdate time.Time    `json:"last_update"`
	Name       string       `json:"name"`
	fetcher    func() (WeaponMatrix, error)
	lastError  error
	sync.Mutex
}

func (ep *getWeaponMatrix) setup(ctx *EndpointCtx) {
	ep.db = ctx.DB

	ctx.Fizz.GET("/api/weapon-matrix", []fizz.OperationOption{
		fizz.Summary("Get 1v1 weapon matrix"),
		fizz.ID("get_weapon_matrix"),
	}, tonic.Handler(ep.handle, 200))

	ep.cache = newCachedMatrix(
		"all",
		func() (WeaponMatrix, error) { return ep.fetchWeaponMatrix(false) },
	)

	ep.slayerCache = newCachedMatrix(
		"slayer",
		func() (WeaponMatrix, error) { return ep.fetchWeaponMatrix(true) },
	)
}

func (ep *getWeaponMatrix) handle(c *gin.Context, params *GetWeaponMatrixParams) (*cachedMatrix, error) {
	var matrix *cachedMatrix
	var err error

	if params.Slayer {
		matrix, err = ep.slayerCache.get()
	} else {
		matrix, err = ep.cache.get()
	}

	if err != nil {
		return nil, errors.Trace(err)
	}

	return matrix, nil
}

func newCachedMatrix(name string, fetcher func() (WeaponMatrix, error)) *cachedMatrix {
	return &cachedMatrix{
		Name:    name,
		fetcher: fetcher,
	}
}

func (c *cachedMatrix) get() (*cachedMatrix, error) {
	if c.Matrix == nil {
		c.update()
	}

	if c.LastUpdate.Before(time.Now().Add(-cacheTTL)) {
		go c.update()
	}

	return c, c.lastError
}

func (c *cachedMatrix) update() {
	c.Lock()

	if c.LastUpdate.Before(time.Now().Add(-cacheTTL)) {
		matrix, err := c.fetcher()
		c.lastError = err
		if err != nil {
			c.lastError = err
			sentry.CaptureException(fmt.Errorf("Failed to fetch weapon matrix: %w", err))
			return
		}
		c.Matrix = matrix
		c.LastUpdate = time.Now()
	}

	c.Unlock()
}

func (ep *getWeaponMatrix) fetchWeaponMatrix(slayerOnly bool) (WeaponMatrix, error) {
	ipClause := "((k.item_power > 900 and k.item_power < 1100 and v.item_power > 900 and v.item_power < 1100) OR (k.item_power > 1200 and v.item_power > 1200))"
	if slayerOnly {
		ipClause = "(k.item_power > 1200 and v.item_power > 1200)"
	}

	query := `
select kl.main_hand_item, vl.main_hand_item, count(*) from events as e
join killers as k on e.event_id = k.event_id and k.is_primary = 1
join victims as v on e.event_id = v.event_id
join loadouts as kl on k.loadout = kl.id
join loadouts as vl on v.loadout = vl.id
WHERE e.participant_count = 1
  and e.party_size = 1
  and %s
  and kl.main_hand_item != "" and vl.main_hand_item != ""
  and e.time > DATE_SUB(NOW(), INTERVAL 7 DAY)
group by kl.main_hand_item, vl.main_hand_item;
`

	rows, err := ep.db.Query(fmt.Sprintf(query, ipClause))

	if err != nil {
		return nil, fmt.Errorf("Failed to weapon matrix query: %v\n", err)
	}

	defer rows.Close()

	matrix := make(WeaponMatrix)

	for rows.Next() {
		var killerWeapon string
		var victimWeapon string
		var count int

		err = rows.Scan(
			&killerWeapon,
			&victimWeapon,
			&count,
		)

		if err != nil {
			return nil, fmt.Errorf("Failed while reading row: %v\n", err)
		}

		if _, ok := matrix[killerWeapon]; !ok {
			matrix[killerWeapon] = make(map[string]*matrixDetail)
		}
		if _, ok := matrix[killerWeapon][victimWeapon]; !ok {
			matrix[killerWeapon][victimWeapon] = &matrixDetail{}
		}
		matrix[killerWeapon][victimWeapon].Wins = count

		if _, ok := matrix[victimWeapon]; !ok {
			matrix[victimWeapon] = make(map[string]*matrixDetail)
		}
		if _, ok := matrix[victimWeapon][killerWeapon]; !ok {
			matrix[victimWeapon][killerWeapon] = &matrixDetail{}
		}
		matrix[victimWeapon][killerWeapon].Losses = count
	}

	return matrix, nil
}
