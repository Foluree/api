package endpoints

import (
	"github.com/gin-gonic/gin"
	"image"
	_ "image/png"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/chai2010/webp"
)

type getItemImageEndpoint struct {
	imageCacheDir string
}

func (ep *getItemImageEndpoint) setup(ctx *EndpointCtx) {
	ctx.GinEngine.GET("/api/items/:name.webp", ep.handle)

	ep.imageCacheDir = os.Getenv("IMAGE_CACHE_DIR")
	if ep.imageCacheDir == "" {
		ep.imageCacheDir = "/data/image-cache"
	}
}

func (ep *getItemImageEndpoint) handle(c *gin.Context) {
	name := c.Param("name.webp")
	quality := c.DefaultQuery("quality", "0")

	nameParts := strings.Split(name, ".")
	itemId := nameParts[0]

	imagePath := ep.imageCacheDir + "/" + quality + "_" + itemId + ".webp"

	if _, err := os.Stat(imagePath); os.IsNotExist(err) {
		ep.saveItemImage(itemId, quality, "60")
	}

	c.Header("Cache-Control", "public, max-age=15552000")
	c.File(imagePath)
}

func (ep *getItemImageEndpoint) saveItemImage(name string, quality, size string) {
	url := "https://render.albiononline.com/v1/item/" + name + ".png?size=" + size + "&quality=" + quality
	response, err := http.Get(url)
	if err != nil {
		log.Printf("Failed to fetch item image: %v\n", err)
		return
	}
	defer response.Body.Close()

	img, _, err := image.Decode(response.Body)

	if err != nil {
		log.Printf("Failed to decode item image: %v\n", err)
		return
	}

	fileName := ep.imageCacheDir + "/" + quality + "_" + name + ".webp"
	file, err := os.Create(fileName)

	if err != nil {
		log.Printf("Failed to create item image file: %v\n", err)
		return
	}
	defer file.Close()

	err = webp.Encode(file, img, nil)

	if err != nil {
		file.Close()
		os.Remove(fileName)
		log.Printf("Failed to encode webp image: %v\n", err)
		return
	}
}
