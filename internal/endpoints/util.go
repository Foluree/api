package endpoints

import (
	"strconv"
	"strings"

	"albion-kills-api/internal/models"
)

func decorateBuildWithItemNames(itemNameMap map[string]string, build *models.Build) {
	build.MainHand.Name = getGenericItemName(build.MainHand.Item, itemNameMap)
	build.OffHand.Name = getGenericItemName(build.OffHand.Item, itemNameMap)
	build.Head.Name = getGenericItemName(build.Head.Item, itemNameMap)
	build.Body.Name = getGenericItemName(build.Body.Item, itemNameMap)
	build.Shoe.Name = getGenericItemName(build.Shoe.Item, itemNameMap)
	build.Cape.Name = getGenericItemName(build.Cape.Item, itemNameMap)
}

func decorateLoadoutWithItemNames(itemNameMap map[string]string, l *models.Loadout) {
	l.MainHand.Name = itemNameMap["T"+strconv.Itoa(l.MainHand.Tier)+"_"+l.MainHand.Item]
	l.OffHand.Name = itemNameMap["T"+strconv.Itoa(l.OffHand.Tier)+"_"+l.OffHand.Item]
	l.Head.Name = itemNameMap["T"+strconv.Itoa(l.Head.Tier)+"_"+l.Head.Item]
	l.Body.Name = itemNameMap["T"+strconv.Itoa(l.Body.Tier)+"_"+l.Body.Item]
	l.Shoe.Name = itemNameMap["T"+strconv.Itoa(l.Shoe.Tier)+"_"+l.Shoe.Item]
	l.Bag.Name = itemNameMap["T"+strconv.Itoa(l.Bag.Tier)+"_"+l.Bag.Item]
	l.Cape.Name = itemNameMap["T"+strconv.Itoa(l.Cape.Tier)+"_"+l.Cape.Item]
	l.Mount.Name = itemNameMap["T"+strconv.Itoa(l.Mount.Tier)+"_"+l.Mount.Item]
	l.Food.Name = itemNameMap["T"+strconv.Itoa(l.Food.Tier)+"_"+l.Food.Item]
	l.Potion.Name = itemNameMap["T"+strconv.Itoa(l.Potion.Tier)+"_"+l.Potion.Item]
}

func getGenericItemName(item string, itemNameMap map[string]string) string {
	t8Name := itemNameMap["T8_"+item]
	return strings.TrimPrefix(t8Name, "Elder's ")
}

func battleSizeToQuery(battleSize string) string {
	switch battleSize {
	case "1v1":
		return is1v1Expr
	case "2v2":
		return is2v2Expr
	case "5v5":
		return is5v5Expr
	case "zvz":
		return isZvZExpr
	default:
		return "(1=1)"
	}
}

func clamp(min, value, max int) int {
	if value > max {
		return max
	}

	if value < min {
		return min
	}

	return value
}
