package endpoints

import (
	"database/sql"
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/juju/errors"
	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"

	"albion-kills-api/internal/models"
	"albion-kills-api/internal/syncstat"
)

type getRating2v2LeaderboardEndpoint struct {
	db *sqlx.DB
}

type GetRating2v2LeaderboardParams struct {
	Season string `query:"season" default:"" description:"Select which ladder season"`
	Search string `query:"q" default:"" description:"Search for player"`
	Weapon string `query:"weapon" default:"" description:"Filter players by weapon"`
	Skip   int    `query:"skip" default:"0" description:"Offset for paging data"`
	Take   int    `query:"take" default:"20" description:"Number of records to fetch" validate:"omitempty,max=100"`
}

func (ep *getRating2v2LeaderboardEndpoint) setup(ctx *EndpointCtx) {
	ep.db = ctx.DB

	ctx.Fizz.GET("/api/leaderboards/2v2", []fizz.OperationOption{
		fizz.Summary("Get 2v2 Rating Leaderboard,"),
		fizz.ID("get_rating_2v2_leaderboard"),
	}, tonic.Handler(ep.handle, 200))
}

func (ep *getRating2v2LeaderboardEndpoint) handle(c *gin.Context, params *GetRating2v2LeaderboardParams) (*models.LeaderboardResponse, error) {
	users, err := ep.fetchPlayers(
		params.Season,
		params.Search,
		params.Skip,
		params.Take,
		params.Weapon,
	)

	if err != nil {
		return nil, errors.Annotate(err, "Failed fetching leaderboard players")
	}

	response := &models.LeaderboardResponse{
		Data:      users,
		Skip:      params.Skip,
		Take:      params.Take,
		SyncDelay: int(syncstat.Status.GetDelay(ep.db).Seconds()),
	}

	return response, nil
}

func (ep *getRating2v2LeaderboardEndpoint) fetchPlayers(
	season string,
	q string,
	skip,
	take int,
	weapon string,
) ([]*models.LeaderboardUser, error) {
	qPred := "(1=1)"
	if q != "" {
		qPred = "(r.name like :q)"
	}
	searchTerm := fmt.Sprintf("%%%s%%", q)

	weapPred := "(1=1)"
	if weapon != "" {
		weapPred = "(fav.weapon = :weapon)"
	}

	tableName := "rating_2v2"
	seasonPred := "(1=1)"
	projection := "r.name, r.rank, r.rating, UNIX_TIMESTAMP(r.last_update), tw.twitch_name, fav.weapon"
	if season != "" {
		tableName = "rating_2v2_history"
		seasonPred = "(r.season = :season)"
		projection = "r.name, r.rank, r.rating, NULL as last_update, tw.twitch_name, r.weapon"
	}

	query := fmt.Sprintf(`
select %s
from %s as r
left join twitch_users as tw on tw.name = r.name
left join player_fav_weapon_2v2 as fav on r.name = fav.name
where r.rank != 0
  and %s
  and %s
  and %s
order by r.rank limit :limit offset :offset`, projection, tableName, seasonPred, weapPred, qPred)

	rows, err := ep.db.NamedQuery(query, map[string]interface{}{
		"limit":  take,
		"offset": skip,
		"season": season,
		"q":      searchTerm,
		"weapon": weapon,
	})

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users := make([]*models.LeaderboardUser, 0)

	for rows.Next() {
		user := &models.LeaderboardUser{}
		var twitchName sql.NullString
		var favWeapon sql.NullString
		var lastUpdate sql.NullInt64
		var rawRating float64

		err = rows.Scan(&user.Name, &user.Rank, &rawRating, &lastUpdate, &twitchName, &favWeapon)

		if err != nil {
			return nil, fmt.Errorf("Failed while reading row: %v\n", err)
		}

		user.Rating = int(rawRating * rating2v2Multiplier)

		if twitchName.Valid {
			user.TwitchUsername = twitchName.String
		}

		if favWeapon.Valid {
			user.FavoriteWeaponItem = favWeapon.String
		}

		if lastUpdate.Valid {
			user.LastUpdate = lastUpdate.Int64
		}

		users = append(users, user)
	}

	return users, nil
}
